package test;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BookingTest {

    Room newRoom=new Room(101,RoomType.SINGLE);
    Guest newGuest=new Guest("Sharad","Sydney",12);
    Date arrivalDate;
    int stayLength=10;
    int occupantNumber=1;
    int roomId=101;
     CreditCard newCard=new CreditCard(CreditCardType.VISA,2,2);
     Booking newBooking;
    List<ServiceCharge> chargesList=new ArrayList<>();
    long confirmationNumber;


    Hotel hotel=new Hotel();
    SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy");

    @BeforeEach
    void setUp() throws Exception{
        format=new SimpleDateFormat("dd-MM-yyyy");
        arrivalDate=format.parse("01-01-2001");
        confirmationNumber=136839745L;
        newBooking=new Booking(newGuest,newRoom,arrivalDate,stayLength,occupantNumber,newCard);
        newBooking.checkIn();



    }


    @AfterEach
    void tearDown() {
    }

    @Test
    final void testAddServiceChargeForBooking() {
        chargesList=newBooking.getCharges();
        newBooking.addServiceCharge(ServiceType.BAR_FRIDGE,200);
        assertEquals(200,chargesList.get(0).getCost());
    }

    @Test
    final void testingCompletionOfCheckout(){
        hotel.activeBookingsByRoomId.put(newBooking.getRoomId(),newBooking);
        hotel.checkout(newRoom.getId());
        assertEquals(0,hotel.activeBookingsByRoomId.size());
    }


}
